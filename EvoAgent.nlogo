turtles-own [age output Spawn life mortality]
patches-own [Patchfit patch-agent]
globals [
  maxseed
  run-counter
  count-tick
  trial-counter
  mutation
  deviation
  sign
  sign2
  how-much
  ;;variables concerning parent population
  maxfitP
  lifeP
  moveP
  reproduceP
  mortalityP
  Peptide-SenseP
  Slowing-factorP
  Budding-ageP
  ;; variables concerning child population
  maxfitC
  lifeC
  moveC
  reproduceC
  mortalityC
  Peptide-SenseC
  Slowing-factorC
  budding-ageC]

;;Creates breeds of each colour and one breed for peptides
Breed[Red-As Red-A]
Breed[Blue-As Blue-A]
Breed[White-As White-A]
Breed[Peptides peptide]


;;BUTTON FUNCTIONS
to setup
  clear-all
  initial-report
  random-seed initial-seed
  set r-seed initial-seed
  set maxfitP (2 * (abs ((max-pxcor + 1) * (min-pycor - 1))))
  set maxfitC (2 * (abs ((max-pxcor + 1) * (min-pycor - 1))))
  set-initial
  create-redI
  create-blueI
  create-whiteI
  create-Pep
  set-parent
  set trial-counter 0
  reset-ticks
  output-print "maximum fitness function = 0"
  set run-counter 0
;  ask one-of turtles with [shape = "circle"] [output-print "Parent Variables"
;  output-print word "Parent Life " LifeP
;  output-print word "Parent movement " MoveP
;  output-print word "Parent Reproduction " ReproduceP
;  output-print word "Parent Mortality " MortalityP
;  output-print word "Parent Peptide Sensitivity " Peptide-SenseP
;  output-print word "Parent Slowing Factor " Slowing-factorP
;  output-print word "Parent budding age " budding-agep]
end

to reset
  set run-counter run-counter + 1
  report-data2
  clear-turtles
  random-seed (r-seed + 1)
  set r-seed (r-seed + 1)
  set count-tick 0
  create-redC
  create-blueC
  create-whiteC
  create-pep
end

to reset2
  set run-counter 0
  set-child
  mutate
  clear-turtles
  random-seed initial-seed
  set r-seed initial-seed
  report-data
  set maxfitC (2 * (abs ((max-pxcor + 1) * (min-pycor - 1))))
  set count-tick 0
  create-redC
  create-blueC
  create-whiteC
  create-pep
  set trial-counter trial-counter + 1
; ask one-of turtles with [shape = "circle"][output-print "Parent Variables"
; output-print  LifeP
;  output-print  MoveP
;  output-print  ReproduceP
;  output-print  MortalityP
;  output-print  Peptide-SenseP
;  output-print  Slowing-FactorP
;  output-print  budding-ageP
;  output-print "Child Variables"
;  output-print  LifeC
;  output-print  MoveC
;  output-print  ReproduceC
;  output-print  MortalityC
;  output-print  Peptide-SenseC
;  Output-print  Slowing-factorC
;  output-print  budding-ageC]
end

to mutate
  random-seed (int new-seed)
  set deviation random 100
  if (deviation >= 0) and (deviation < 75) [set how-much 1]
  if (deviation >= 75) and (deviation < 90) [set how-much 2]
  if (deviation >= 90) and (deviation < 95) [set how-much 3]
  if (deviation >= 95) and (deviation < 98) [set how-much 4]
  if (deviation >= 98) and (deviation < 100) [set how-much 5]
  set sign random 2
  if sign = 0 [set sign2 1]
  if sign = 1 [set sign2 -1]
  set how-much (how-much * sign2)
  set mutation random 7
  if mutation = 0 [ifelse (sign2 = -1) and (lifeC > (-1 * how-much)) [set lifeC (lifeC + how-much) set lifec precision lifec 0][mutate]
                   if sign2 = 1 [set lifeC (lifeC + how-much) set lifec precision lifec 0]]
  if mutation = 1 [ifelse (sign2 = -1) and (moveC > (-1 * how-much))[set moveC (MoveC + how-much) set movec precision movec 0][mutate]
                   if sign2 = 1 [set moveC (moveC + how-much)] set movec precision movec 0]
  if mutation = 2 [ifelse (sign2 = -1) and (reproduceC > (-1 * 0.05 * how-much))[set reproduceC (reproduceC + (0.05 * how-much)) set reproducec precision reproducec 2][mutate]
                   if sign2 = 1 [set reproduceC (reproduceC + (0.05 * how-much)) set reproducec precision reproducec 2]]
  if mutation = 3 [ifelse (sign2 = -1) and (MortalityC > (-1 * 0.05 * how-much))[set MortalityC (MortalityC + (0.05 * how-much)) set mortalityc precision mortalityc 2][mutate]
                   if sign2 = 1 [set MortalityC (MortalityC + (0.05 * how-much)) set mortalityc precision mortalityc 2]]
  if mutation = 4 [ifelse (sign2 = -1) and (Peptide-senseC > (-1 * how-much))[set Peptide-senseC (Peptide-senseC + how-much) set peptide-senseC precision peptide-sensec 0][mutate]
                   if sign2 = 1 [set Peptide-senseC (Peptide-senseC + how-much) set peptide-senseC precision peptide-sensec 0]]
  if mutation = 5 [ifelse (sign2 = -1) and (Slowing-factorC > (-1 * 0.05 * how-much))[set Slowing-factorC (Slowing-factorC + ( 0.05 * how-much)) set slowing-factorc precision slowing-factorc 2][mutate]
                   if sign2 = 1 [set Slowing-factorC (Slowing-factorC + ( 0.05 * how-much)) set slowing-factorc precision slowing-factorc 2]]
  if mutation = 6 [ifelse (sign2 = -1) and (budding-ageC > (-1 * how-much)) [set budding-ageC (budding-ageC + how-much) set budding-agec precision budding-agec 0][mutate]
                   if sign2 = 1 [set budding-ageC (budding-ageC + how-much) set budding-agec precision budding-agec 0]]
  random-seed initial-seed
end

to clear
   clear-all
    reset-ticks
end


;;CREATION PROCEDURES
to set-parent
    set lifeP LifeC
    set moveP MoveC
    set reproduceP ReproduceC
    set mortalityP MortalityC
    set Peptide-senseP Peptide-senseC
    set Slowing-factorP Slowing-factorC
    set budding-ageP budding-ageC
end
to create-redI
  create-Red-As Red-Agents ;;Creates breed specific turtles
    [setxy (int random-xcor) (int random-ycor)
    set color red
    set shape "circle"
    set heading 0
    set age 0]
end

to create-blueI
  create-Blue-As Blue-Agents
   [setxy (int random-xcor) (int random-ycor)
    set color blue
    set shape "circle"
    set heading 0
    set age 0]
end

to create-whiteI
  create-White-As White-Agents
   [setxy (int random-xcor) (int random-ycor)
    set color white
    set shape "circle"
    set heading 0
    set age 0]
end

to set-Initial
    set lifeC Lifespan
    set moveC move-distance
    set reproduceC Reproduction-Chance
    set mortalityC Mortality-Rate
    set Peptide-senseC (sensitivity)
    set Slowing-FactorC Slowing-factor
    set budding-ageC budding-age
end

to create-redC
  create-Red-As Red-Agents ;;Creates breed specific turtles
    [setxy (int random-xcor) (int random-ycor)
    set color red
    set shape "circle"
    set heading 0
    set age 0]
end

to create-blueC
  create-Blue-As Blue-Agents
   [
    setxy (int random-xcor) (int random-ycor)
    set color blue
    set shape "circle"
    set heading 0
    set age 0]
end

to create-whiteC
  create-White-As White-Agents
   [
    setxy (int random-xcor) (int random-ycor)
    set color white
    set shape "circle"
    set heading 0
    set age 0]
end

to set-Child
    set lifeC LifeP
    set moveC MoveP
    set reproduceC ReproduceP
    set mortalityC MortalityP
    set Peptide-senseC Peptide-senseP
    set Slowing-factorC Slowing-factorP
    set budding-ageC budding-ageP
end


to Create-Pep
   Create-Peptides Initial-Peptides
      [setxy random-xcor random-ycor
      set color gray
      hide-turtle
      set life Peptide-Lifespan
      set age 0
      set mortality 1
      set heading random 360]
end

to set-Peptides
      sprout-Peptides Peptide-Creation
      [hide-turtle
      set life Peptide-Lifespan
      set age 0
      set mortality 1
      set heading random 360]
end


to go
  if (run-counter >= number-runs) [ output-print word "Maximum fitness of run = " maxfitC output-print word "From seed: " maxseed;; needs adjusting - do we still need this?
  if maxfitC <= maxfitP [(set maxfitP maxfitC) set-parent] reset2]
  if (maxfitp <= desired-fitness) [report-data stop]
  if (count-tick = trial-length) and (count-tick > 0) [if fitness <= maxfitC [set maxfitC fitness set maxseed r-seed]
     output-print word "seed number: " r-seed output-print word  "current fitness: " fitness output-print word "maximum fitness: " maxfitC
      reset]
  set count-tick count-tick + 1
  check-population       ;; check if all breeds in pop are still alive. If not restart
  check-life             ;; check if lifespan is exceeded and chance to die if so
  check-reproduce        ;; check if need to reproduce, and if so do
  check-peptides         ;; check the concentration of peptides in the environment
  move-agents            ;; checks surrounding and moves agents accordingly
  Pep-Move               ;; moves peptides
  check-fitness          ;;calculates and checks fitness [this could be changed to a function at the end?]
  tick
end

;;GO PROCEDURES

to check-population
  if not any? Blue-as [output-print "Breed death - Blue " output-print "Ticks in Run: " output-print (count-tick) report-data3 reset]
  if not any? Red-as [output-print "Breed death - Red " output-print "Ticks in Run: " output-print (count-tick) report-data4 reset]
  if not any? White-as [output-print "Breed death - White " output-print "Ticks in Run: " output-print (count-tick) report-data5 reset]
end

to check-life ;; checks the life of the turtles and throws it to the mortality counter
  ask turtles
  [if age >= lifec [check-mortality]]
end

to check-reproduce ;;if turtle is 'mature' enough has a chance to reproduce set by reproduction slider
  ask turtles
  [if age >= budding-agec
    [if reproduceC >= random-float 1
  [reproduce-compass]]]
end

to check-peptides ;;agent recognition of peptide number in environment
  ask turtles with [shape = "circle"]
  [if count peptides in-radius 2 >= sensitivity [ask patch-here [set-peptides]]]
end

to check-fitness ;; a empty is greater than wrong which is greater than correct.
  ask patches with [pxcor < ((1 / 3) * max-pxcor )][if (count blue-as in-radius 0.9) = 1 [set patchfit 0]
    if (count red-as in-radius 0.9 = 1) or (count white-as in-radius 0.9 = 1) [set patchfit 1]
    if (count turtles with [shape = "circle"] in-radius 0.9) = 0 [set patchfit 2]]
  ask patches with [(pxcor >= ((1 / 3) * max-pxcor)) and (pxcor < ((2 / 3) * max-pxcor))][if (count white-as in-radius 0.9) = 1 [set patchfit 0]
    if (count red-as in-radius 0.9 = 1) or (count blue-as in-radius 0.9 = 1) [set patchfit 1]
    if (count turtles with [shape = "circle"] in-radius 0.9) = 0 [set patchfit 2]]
  ask patches with [pxcor >= ((2 / 3) * max-pxcor)][if (count red-as in-radius 0.9 = 1) [set patchfit 0]
    if (count blue-as in-radius 0.9 = 1) or (count white-as in-radius 0.9 = 1) [set patchfit 1]
    if (count turtles with [shape = "circle"] in-radius 0.9) = 0 [set patchfit 2]]
end

to-report fitness
  report sum ([patchfit] of patches)
end

;;MOVEMENT

to move-agents ;;now recognise surrounding peptides and alter movement accordingly
  ask Red-as
 [ifelse check-area = (count Red-as in-radius 1.9)[move-less-compass][move-compass]]

  ask Blue-As
  [ifelse check-area = (count Blue-as in-radius 1.9)[move-less-compass][move-compass]]

  ask White-As
  [ifelse check-area = (count White-as in-radius 1.9)[move-less-compass][move-compass]]
  ask turtles
  [set age age + 1]
end

to-report check-area
  report max (list (count red-as in-radius 1.0) (count blue-as in-radius 1.9) (count white-as in-radius 1.9))
end

to move-compass
  repeat moveC
  [check-agents downhill patch-agent]
end

to move-less-compass
  repeat (moveC * slowing-factorC)
  [check-agents downhill patch-agent]
end

;;PEPTIDE MOVEMENT - POSSIBLY NEEDS TO BE A LITTLE MORE SOPHISTICATED?

to Pep-Move ;;peptide dispersal
  ask Peptides [right random 360 Fd 1]
end


;;VARIABLES THAT ARE DRAWN UPON

to check-mortality
  if mortalityc >= random-float 1 [die]
end

to-report run-number
  if (trial-length > 0) [report (trial-length * number-runs)]
end

to reproduce-compass
  set Spawn random 9
  if Spawn = 0 []
  if Spawn = 1 [ifelse ycor <= (max-pycor - 1) [ifelse (any? turtles with [shape = "circle"] at-points [[0 1]]) = false [set age 0 hatch 1 [set xcor int xcor set ycor ((int ycor) + 1)]][reproduce-compass]][]]
  if Spawn = 2 [ifelse (xcor <= (max-pxcor - 1)) and (ycor <= (max-pycor - 1)) [ifelse (any? turtles with [shape = "circle"] at-points [[1 1]]) = false [set age 0 hatch 1 [set xcor ((int xcor) + 1) set ycor ((int ycor) + 1)]][reproduce-compass]][]]
  if Spawn = 3 [ifelse xcor <= (max-pxcor - 1)[ifelse (any? turtles with [shape = "circle"] at-points [[1 0]]) = false [set age 0 hatch 1 [set xcor ((int xcor) + 1)  set ycor int ycor]][reproduce-compass]][]]
  if Spawn = 4 [ifelse (xcor <= (max-pxcor - 1)) and (ycor >= (min-pycor + 1))[ifelse (any? turtles with [shape = "circle"] at-points [[1 -1]]) = false [set age 0 hatch 1 [set xcor ((int xcor) + 1) set ycor ((int ycor) - 1)]][reproduce-compass]][]]
  if Spawn = 5 [ifelse ycor >= (min-pycor + 1) [ifelse (any? turtles with [shape = "circle"] at-points [[0 -1]]) = false [set age 0 hatch 1 [set xcor int xcor set ycor ((int ycor) - 1)]][reproduce-compass]][]]
  if Spawn = 6 [ifelse (xcor >= (min-pxcor + 1)) and (ycor >= (min-pycor + 1)) [ifelse (any? turtles with [shape = "circle"] at-points [[-1 -1]]) = false [set age 0 hatch 1 [set xcor ((int xcor) - 1) set ycor ((int ycor) - 1)]][reproduce-compass]][]]
  if Spawn = 7 [ifelse xcor >= (min-pxcor + 1) [ifelse (any? turtles with [shape = "circle"] at-points [[-1 0]]) = false [set age 0 hatch 1 [set xcor ((int xcor) - 1) set ycor int ycor]][reproduce-compass]][]]
  if Spawn = 8 [ifelse (xcor >= (min-pxcor + 1)) and (ycor <= (max-pycor - 1)) [ifelse (any? turtles with [shape = "circle"] at-points [[-1 1]]) = false [set age 0 hatch 1 [set xcor ((int xcor) - 1) set ycor ((int ycor) + 1)]][reproduce-compass]][]]
end

to check-agents
  ask patch-here [ifelse any? turtles with [shape = "circle"] in-radius 0.9 [set patch-agent 1][set patch-agent 0]]
  ask neighbors [ifelse any? turtles with [shape = "circle"] in-radius 0.9 [set patch-agent 1][set patch-agent 0]]
end

to parent ;; a button that will be made into a variable/should we keep the button to print variables when we want them?
  ask one-of turtles with [shape = "circle"][output-print "Parent Variables"
  output-print word "Parent Life " LifeP
  output-print word "Parent movement " MoveP
  output-print word "Parent Reproduction " ReproduceP
  output-print word "Parent Mortality " MortalityP
  output-print word "Parent Peptide Sensitivity " Peptide-SenseP
  output-print word "Parent Slowing Factor" Slowing-factorP
  output-print word "Parent budding-age " budding-ageP]
end

to Child ;; a button that will be made into a variable/should we keep the button to print variables when we want them?
  ask one-of turtles with [shape = "circle"]
  [output-print "Child Variables"
  output-print word "Child Life " LifeC
  output-print word "Child movement " MoveC
  output-print word "Child Reproduction " ReproduceC
  output-print word "Child Mortality " MortalityC
  output-print word "Child Peptide Sensitivity " Peptide-SenseC
  output-print word "Child Slowing Factor" Slowing-factorC
  output-print word "Child budding age "budding-ageC]
end

to initial-report
  ifelse (file-exists? "data.csv")
   [file-open "data.csv"
     file-type "Trial Counter"
     file-type ","
     file-type "R seed number"
     file-type ","
     file-type "Parent maxfitness"
     file-type ","
     file-type "Parent Life"
     file-type ","
     file-type "Parent movement"
     file-type ","
     file-type "Parent reproduction"
     file-type ","
     file-type "Parent mortality"
     file-type ","
     file-type "Parent peptide sensitivity"
     file-type ","
     file-type "Parent slowing factor"
     file-type ","
     file-type "Parent budding age"
     file-type ",,"
     file-type "Child maxfitness"
     file-type ","
     file-type "Child Life"
     file-type ","
     file-type "Child movement"
     file-type ","
     file-type "Child reproduction"
     file-type ","
     file-type "Child mortality"
     file-type ","
     file-type "Child peptide sensitivity"
     file-type ","
     file-type "Child slowing factor"
     file-type ","
     file-print "Child budding age"
     file-close]
   [print "ERROR 'DATA.CSV' NOT PRESENT - DATA NOT BEING REPORTED"
      print "Please make sure filename 'data.csv' exists in current directory"]
end

to report-data
  ifelse (file-exists? "data.csv")
    [file-open "data.csv"
      file-type trial-counter
      file-type ",,"
      file-type maxfitp
      file-type ","
      file-type lifep
      file-type ","
      file-type movep
      file-type ","
      file-type reproducep
      file-type ","
      file-type mortalityp
      file-type ","
      file-type peptide-sensep
      file-type ","
      file-type slowing-factorp
      file-type ","
      file-type budding-agep
      file-type ",,,"
      file-type lifec
      file-type ","
      file-type movec
      file-type ","
      file-type reproducec
      file-type ","
      file-type mortalityc
      file-type ","
      file-type peptide-sensec
      file-type ","
      file-type slowing-factorc
      file-type ","
      file-print budding-agec
      file-close ]
    [print "ERROR 'DATA.CSV' NOT PRESENT - DATA NOT BEING REPORTED"
      print "Please make sure filename 'data.csv' exists in current directory"]
end

to report-data2
   ifelse (file-exists? "data.csv")
    [file-open "data.csv"
      file-type ","
      file-type r-seed
      file-type ",,,,,,,,,,"
      file-print fitness]
    [print "ERROR 'DATA.CSV' NOT PRESENT - DATA NOT BEING REPORTED"
      print "Please make sure filename 'data.csv' exists in current directory"]
end

to report-data3
   ifelse (file-exists? "data.csv")
    [file-open "data.csv"
      file-type ","
      file-type r-seed
      file-type ",,,,"
      file-type "Breed Death - Blue"
      file-type ",,,,"
      file-type "Ticks in run"
      file-type ",,,"
      file-print count-tick]
    [print "ERROR 'DATA.CSV' NOT PRESENT - DATA NOT BEING REPORTED"
      print "Please make sure filename 'data.csv' exists in current directory"]
end

to report-data4
   ifelse (file-exists? "data.csv")
    [file-open "data.csv"
      file-type ","
      file-type r-seed
      file-type ",,,,"
      file-type "Breed Death - Red"
      file-type ",,,,"
      file-type "Ticks in run"
      file-type ",,,"
      file-print count-tick]
    [print "ERROR 'DATA.CSV' NOT PRESENT - DATA NOT BEING REPORTED"
      print "Please make sure filename 'data.csv' exists in current directory"]
end

to report-data5
   ifelse (file-exists? "data.csv")
    [file-open "data.csv"
      file-type ","
      file-type r-seed
      file-type ",,,"
      file-type "Breed Death - White"
      file-type ",,,,"
      file-type "Ticks in run"
      file-type ",,,"
      file-print count-tick]
    [print "ERROR 'DATA.CSV' NOT PRESENT - DATA NOT BEING REPORTED"
      print "Please make sure filename 'data.csv' exists in current directory"]
end
@#$#@#$#@
GRAPHICS-WINDOW
588
10
1040
483
-1
-1
13.0
1
10
1
1
1
0
0
0
1
0
33
-33
0
1
1
1
minutes
30.0

SLIDER
40
42
212
75
Red-Agents
Red-Agents
0
100
15
1
1
NIL
HORIZONTAL

SLIDER
39
75
211
108
Blue-Agents
Blue-Agents
0
100
15
1
1
NIL
HORIZONTAL

SLIDER
40
118
212
151
White-Agents
White-Agents
0
100
15
1
1
NIL
HORIZONTAL

BUTTON
486
330
552
363
Setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
491
193
554
226
Go
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

BUTTON
488
283
552
316
Clear
clear-all
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
454
241
554
274
Go Forever
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
40
246
212
279
Peptide-Lifespan
Peptide-Lifespan
0
100
0
1
1
NIL
HORIZONTAL

SLIDER
40
160
212
193
Move-Distance
Move-Distance
0
100
8
1
1
NIL
HORIZONTAL

SLIDER
40
203
212
236
Lifespan
Lifespan
0
100
22
1
1
NIL
HORIZONTAL

MONITOR
494
19
577
64
Red Turtles
count Red-As
17
1
11

MONITOR
494
79
580
124
Blue Turtles
count Blue-As
17
1
11

MONITOR
492
139
582
184
White turtles
count White-As
17
1
11

SLIDER
34
287
224
320
Reproduction-Chance
Reproduction-Chance
0
1
0.35
0.05
1
NIL
HORIZONTAL

INPUTBOX
504
437
577
497
Number-runs
2
1
0
Number

INPUTBOX
515
369
568
429
r-seed
2
1
0
Number

SLIDER
32
330
204
363
Mortality-Rate
Mortality-Rate
0
1.0
0.6
0.05
1
NIL
HORIZONTAL

MONITOR
264
23
368
68
Total Turtle counter
count turtles
17
1
11

SLIDER
33
370
205
403
Peptide-Creation
Peptide-Creation
0
10
3
1
1
NIL
HORIZONTAL

MONITOR
275
143
365
188
Peptide Counter
count peptides
17
1
11

MONITOR
260
80
367
125
Agent Counter
Count turtles with [shape = \"circle\"]
17
1
11

PLOT
224
209
424
359
Fitness
ticks
fitness
0.0
10.0
0.0
3.0
true
false
"" ""
PENS
"fitness" 1.0 0 -16777216 true "" "plot fitness"

OUTPUT
1050
18
1290
482
11

SLIDER
34
417
206
450
Initial-Peptides
Initial-Peptides
0
1000
0
10
1
NIL
HORIZONTAL

SLIDER
35
455
207
488
Sensitivity
Sensitivity
0
100
13
1
1
NIL
HORIZONTAL

INPUTBOX
429
439
496
499
trial-length
1000
1
0
Number

INPUTBOX
587
484
671
544
Budding-age
20
1
0
Number

INPUTBOX
434
370
511
430
initial-seed
1
1
0
Number

SLIDER
40
10
212
43
slowing-factor
slowing-factor
0
1
0.25
0.05
1
NIL
HORIZONTAL

BUTTON
399
103
468
136
NIL
Parent
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
402
151
465
184
NIL
Child
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
682
484
767
544
desired-fitness
600
1
0
Number

PLOT
224
366
424
516
Maximum fitness
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot maxfitP"

BUTTON
388
58
476
91
Save data
export-output user-new-file
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

invisible
true
0

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.3
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
